import { Component } from '@angular/core';

export interface People {
  name: string
  surname: string
  telephone: string
  id?: number
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  search = "";

  people: People[] = [
    {name: "Имя1", surname: "Фамилия1", telephone: "(111) 111-1111", id: 1},
    {name: "Имя2", surname: "Фамилия2", telephone: "(222) 222-2222", id: 2},
    {name: "Имя3", surname: "Фамилия3", telephone: "(333) 333-3333", id: 3},
  ]

  updatePeople(p: People) {
    if (this.people.length == 0) {
      p.id = 1;
    } else p.id = this.people[this.people.length-1].id + 1 ;
    this.people.push(p);
  }

  deletePeople(p: People) {
    let index = this.people.findIndex((el)=>el.id==p.id)
    this.people.splice(index, 1);
  }

  savePeople(p: People) {
    let index = this.people.findIndex((el)=>el.id==p.id)
    this.people.splice(index, 1, p);
  }
}

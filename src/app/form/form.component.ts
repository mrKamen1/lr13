import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { People } from '../app.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
})
export class FormComponent implements OnInit {
  form : FormGroup;

  @Output() onAdd: EventEmitter<People> = new EventEmitter<People>()

  name = "";
  surname = "";
  telephone = "";
  disabled = true;
  public myModel = '';
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

  constructor() { }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(1)]),
      surname: new FormControl('', [Validators.required, Validators.minLength(1)]),
      telephone: new FormControl('', [Validators.required, Validators.minLength(11)])
    });
  }

  submit() {
    const formData = {...this.form.value}
    this.disabled = false;
    this.onAdd.emit({name: formData.name, surname: formData.surname, telephone: formData.telephone});
    this.form.reset()
  }
}
